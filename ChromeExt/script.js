// ==UserScript==
// @name         LigaScript
// @namespace    http://ligarobotov.s20.online/
// @version      0.1
// @include     http://*
// @include     https://*
// @description  Автоматический подсчет строки Бонус в Альфа-CRM
// @author       stas12312
// ==/UserScript==
(function() {
    'use strict';

    // Your code here...
    let re = /^[0-9\+\-\*\/]/;
    function calculateLigaBits(e){
        if(e.target.TagName === 'input' && e.target.getAttribute('placeholder') !== 'Примечание'){return false}
        let calculate_string = e.target.value;
		if (!re.test(calculate_string) && calculate_string !== ''){
            return false;
        }
        try {
            let tr = e.target.parentNode.parentNode;
            let result = eval(calculate_string);
            let result_input = tr.childNodes[5].childNodes[1].childNodes[3].childNodes[1];
            if(result === undefined) result = '';
            result_input.value = result;
        }
        catch(ValueError){
            return false;
        }

    }
    document.addEventListener('input', calculateLigaBits);
})();